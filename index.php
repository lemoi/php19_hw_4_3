<?php
/* Домашнее задание к лекции 4.3 «SELECT из нескольких таблиц»

За основу взять задание со второй лекции — наше TODO-приложение, и дополнить его:
    1. Добавить регистрацию и авторизацию.
    2. Добавить возможность закреплять задачу за другим пользователем в системе.
    3. Выводить для пользователя 2 списка задач: созданные им и, отдельно, задачи, закрепленные за ним другими пользователями.

Детали домашнего задания более подробно разобраны во время лекции, поскольку задание достаточно сложное. Рекомендую еще раз пересмотреть перед выполнением.

Пример выполнения (http://university.netology.ru/u/vfilipov/l3/index.php).

Дамп базы данных для выполнения задания (https://netology-university.bitbucket.io/php/homework/4.3-join/dump.txt).
*/

require_once 'core/function.php';

$users = getUsers();

$action = getParamGet('action');
$id = (int)getParamGet('id');
$description = '';

if ($action == 'delete') {
    deleteTask($id);
} elseif ($action == 'edit') {
    $task = getTask($id);
    $description = $task['description'];
    $_SESSION['taskId'] = $task['id'];
} elseif ($action == 'done') {
    $task = getTask($id);
    if ($task['is_done']) {
        resumeTask($id);
    } else {
        completeTask($id);
    }
    redirect('index');
}

$order = 'date_added';
if (isPost()) {
    if (array_key_exists('save', $_POST) && !empty(getParamPost('description'))) {
        if (!empty($id) && getParamPost('save') == 'Сохранить') {
            updateTask($id, getParamPost('description'));
            unset($_SESSION['taskId']);
            redirect('index');

        } elseif (getParamPost('save') == 'Добавить') {
            createTask(getParamPost('description'));
        }

    } elseif (array_key_exists('sort', $_POST)) {
        $order = getParamPost('sort_by');

    } elseif (array_key_exists('assign', $_POST)) {
        $arg = explode('_', getParamPost('assigned_user_id'));
        if (count($arg) == 4) {
            assignTask($arg[3], $arg[1]);
        }
    }
}

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>PHP-19. Task 4.3</title>
    <style>
        table {
            border-spacing: 0;
            border-collapse: collapse;
            margin-top: 10px;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }
    </style>
</head>
<body>
<?php
$message = 'Введите данные для регистрации или войдите, если уже регистрировались:';
if (isPost()) {
    $login = trim(getParamPost('login'));
    $password = trim(getParamPost('password'));

    $register = array_key_exists('register', $_POST);
    $sign_in = array_key_exists('sign_in', $_POST);
    if ($register || $sign_in) {
        if (empty($login) || empty($password)) {
            $message = 'Поля логин и пароль обязательны для заполнения.';

        } elseif ($register) {
            $result = createUser($login, $password);
            if ($result == true) {
                $_SESSION['userId'] = $result['id'];
                $message = '';
            } else {
                $message = 'Такой пользователь уже существует в базе данных.';
            }

        } elseif ($sign_in) {
            $result = getUser($login, $password);
            if ($result == false) {
                $message = 'Такой пользователь не существует, либо неверный пароль.';
            } else {
                $_SESSION['userId'] = $result['id'];
                $message = '';
            }
        }
    }
}

if (empty(getParamSession('userId'))) {
    ?>
    <p><?= $message ?></p>
    <form method="POST">
        <input type="text" name="login" placeholder="Логин" value="<?= getParamPost('login') ?>"/>
        <input type="password" name="password" placeholder="Пароль"/>
        <input type="submit" name="sign_in" value="Вход"/>
        <input type="submit" name="register" value="Регистрация"/>
    </form>
    <?php
} else {
    ?>
    <h1>Список дел на сегодня</h1>
    <div style="float: left">
        <form method="POST">
            <input type="text" name="description" placeholder="Описание задачи" value="<?= $description ?>"/>
            <input type="submit" name="save"
                   value="<?= getParamSession('taskId') == null ? 'Добавить' : 'Сохранить' ?>"/>
        </form>
    </div>
    <div style="float: left; margin-left: 20px;">
        <form method="POST">
            <label for="sort">Сортировать по:</label>
            <select name="sort_by">
                <option <?= $order == 'date_added' ? 'selected ' : '' ?>value="date_added">Дате добавления</option>
                <option <?= $order == 'is_done' ? 'selected ' : '' ?>value="is_done">Статусу</option>
                <option <?= $order == 'description' ? 'selected ' : '' ?>value="description">Описанию</option>
            </select>
            <input type="submit" name="sort" value="Отсортировать"/>
        </form>
    </div>
    <div style="clear: both"></div>

    <table>
        <tr>
            <th>Описание задачи</th>
            <th>Дата добавления</th>
            <th>Статус</th>
            <th></th>
            <th>Ответственный</th>
            <th>Автор</th>
            <th>Закрепить задачу за пользователем</th>
        </tr>
        <?php

        //$result = getTasks($order);
        $result = getTasks($order, 'own');
        foreach ($result as $row) {
            ?>
            <tr>
                <td><?= $row['description'] ?></td>
                <td><?= $row['date_added'] ?></td>
                <td><?= $row['is_done'] ? "<span style='color: green;'>Выполнено</span>" : "<span style='color: orange;'>В процессе</span>" ?></td>
                <td>
                    <a href="<?= "?id={$row['id']}&action=edit" ?>">Изменить</a>
                    <a href="<?= "?id={$row['id']}&action=done" ?>"><?= $row['is_done'] ? 'Возобновить' : 'Выполнить' ?></a>
                    <?php if ($row['assigned_user_id'] === NULL || $row['assigned_user_id'] == getParamSession('userId')) { ?>
                        <a href="<?= "?id={$row['id']}&action=delete" ?>">Удалить</a>
                    <?php } ?>
                </td>
                <td><?= $row['responsible'] ?></td>
                <td><?= $row['author'] ?></td>
                <td>
                    <form method='POST'>
                        <select name='assigned_user_id'>
                            <?php
                            foreach ($users as $user) {
                                echo "<option value='user_${user['id']}_task_${row['id']}'>${user['login']}</option>";
                            }
                            ?>
                        </select>
                        <input type='submit' name='assign' value='Переложить ответственность'/>
                    </form>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <p><strong>Также, посмотрите, что от Вас требуют другие люди:</strong></p>
    <table>
        <tr>
            <th>Описание задачи</th>
            <th>Дата добавления</th>

            <th>Статус</th>
            <th></th>
            <th>Ответственный</th>
            <th>Автор</th>
        </tr>
        <?php

        $result = getTasks($order, 'assigned');
        foreach ($result as $row) {
            ?>
            <tr>
                <td><?= $row['description'] ?></td>
                <td><?= $row['date_added'] ?></td>
                <td><?= $row['is_done'] ? "<span style='color: green;'>Выполнено</span>" : "<span style='color: orange;'>В процессе</span>" ?></td>
                <td>
                    <a href="<?= "?id={$row['id']}&action=edit" ?>">Изменить</a>
                    <a href="<?= "?id={$row['id']}&action=done" ?>"><?= $row['is_done'] ? 'Возобновить' : 'Выполнить' ?></a>
                    <a href="<?= "?id={$row['id']}&action=delete" ?>">Удалить</a>
                </td>
                <td><?= $row['responsible'] ?></td>
                <td><?= $row['author'] ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <p><a href="logout.php">Выход</a></p>
    <?php
}
?>
</body>
</html>
